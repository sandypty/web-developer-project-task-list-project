<?php
    require_once("ITTaskManager.php");
    require_once("Task.php");
    
    class TaskManager implements ITTaskManager
    {
        public function create($desc)
        {
            $db = new PDO("mysql:host=localhost;dbname=task", "root", "root");
            
            $sql = "INSERT INTO tasktable (`desc`) VALUES (:desc)";
            
            try
            {
                $query = $db->prepare($sql);
                $query->bindParam(":desc", $desc);
                $query->execute();
            }
            catch(Exception $ex)
            {
                echo "{$ex->getMessage()}<br/>";
                
            } 
                return $db->lastInsertId();
        }
        
        
        public function readById($id)
        {
        
            $db = new PDO("mysql:host=localhost;dbname=task", "root", "root");

            $sql = "SELECT FROM tasktable WHERE id=:id";
            $rowsAffected = 0;
            
            try
            {
                $query = $db->prepare($sql);
                $query->bindParam(":id", $id);
                $query->bindParam(":desc", $desc);
                $query->execute();
                $rowsAffected = $query->rowCount();
            
            }
            catch(Exception $ex)
            {
                echo "{$ex->getMessage()}<br/>";
                
            } 
                return $rowsAffected;
        }
        
        
        public function readAll()
        {
            $db = new PDO("mysql:host=localhost;dbname=task", "root", "root");
            
            $sql = "SELECT * FROM tasktable";
            
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO:: ERRMODE_EXCEPTION);
            
            try
            {
                $query = $db->prepare($sql);
                $query->execute();
                
                $results = $query->fetchAll(PDO::FETCH_CLASS, "Task");
            }
            catch(Exception $ex)
            {
                echo "{$ex->getMessage()}<br/>";
                
            } 
               foreach($results as $task)
               {
                echo $task;
               }
        }
        
        
        public function update($id, $newDesc)
        {
            $db = new PDO("mysql:host=localhost;dbname=task", "root", "root");
            
            $sql = "UPDATE tasktable SET `desc`=:desc WHERE `id`=:id";
            
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO:: ERRMODE_EXCEPTION);
            
            $rowsAffected = 0;
            
            try
            {
                $query = $db->prepare($sql);
                $query->bindParam(":id", $id);
                $query->bindParam(":desc", $newDesc);
                $query->execute();
                $rowsAffected = $query->rowCount();
            
            }
            catch(Exception $ex)
            {
                echo "{$ex->getMessage()}<br/>";
                
            } 
                return $rowsAffected;
        }
        
        
        public function delete($id)
        {
            $db = new PDO("mysql:host=localhost;dbname=task", "root", "root");

            $sql = "DELETE FROM tasktable WHERE id=:id";
            $rowsAffected = 0;
            
            try
            {
                $query = $db->prepare($sql);
                $query->bindParam(":id", $id);
                $query->execute();
                $rowsAffected = $query->rowCount();
            
            }
            catch(Exception $ex)
            {
                echo "{$ex->getMessage()}<br/>";
                
            } 
                return $rowsAffected;
        }
    
    }
        
        
?>
