<?php
    interface ITTaskManager
    {
        public function create($desc);
	    public function readById($id);
	    public function readAll();
	    public function update($id, $newDesc);
	    public function delete($id);
    
    }

?>
