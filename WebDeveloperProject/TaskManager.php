<?php
    class TaskManager 
    {
        public function create($task, $completion)
        {
            $retVal = null;
            
            $db = new PDO("mysql:host=localhost;dbname=Task_List", "root", "root");
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            
            $sql = "INSERT INTO taskTable(task, completion) VALUES(:task, :completion)";
        
            try 
            {
                $query = $db->prepare($sql);
                $query->execute(array(":task"=>$task, "completion"=>$completion));
                
                $retyVal = $db->lastInsertId();
            }
            catch(Exception $ex)
            {
                echo "{$ex->getMessage()}<br/>\n";
            }
            
            return $retVal;
        }
        
        public function read()
        {
            $retVal = null;
            
            $db = new PDO("mysql:host=localhost;dbname=Task_List", "root", "root");
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            
            $sql = "SELECT id, task, completion FROM taskTable";
            
            try
            {
                $query = $db->prepare($sql);
                $query->execute();
                
                $results = $query->fetchAll(PDO::FETCH_ASSOC);
                
                $retVal = json_encode($results, JSON_PRETTY_PRINT);
            }
            catch(Exception $ex)
            {
                echo "{$ex->getMessage()}<br/>\n";
            }
            
            return $retVal;
        }
        
        public function readById($id)
        {
            $retVal = null;
            
            $db = new PDO("mysql:host=localhost;dbname=Task_List", "root", "root");
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            
            $sql = "SELECT task, completion FROM taskTable WHERE id=:id";
            
            try
            {
                $query = $db->prepare($sql);
                $query->bindParam(":id", $id);
                $query->execute();
                
                $results = $query->fetchAll(PDO::FETCH_ASSOC);
                
                $retVal = json_encode($results, JSON_PRETTY_PRINT);
            }
            catch(Exception $ex)
            {
                echo "{$ex->getMessage()}<br/>\n";
            }
            
            return $retVal;
        }
        
        public function update($id, $task, $completion)
        {
            $retVal = null;
            
            $db = new PDO("mysql:host=localhost;dbname=Task_List", "root", "root");
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            
            $sql = "UPDATE taskTable SET task=:task, completion=:completion WHERE id=:id";
            
            try
            {
                $query = $db->prepare($sql);
                $query->execute(array(":id"=>$id, ":task"=>$task, ":completion"=>$completion));
                
                $retVal = $query->rowCount();
            }
            catch(Exception $ex)
            {
                echo "{$ex->getMessage()}<br/>\n";
            }
            
            return $retVal;
            
        }
        
        public function delete($id)
        {
            $retVal = null;
            
            $db = new PDO("mysql:host=localhost;dbname=Task_list", "root", "root");
            $db->setAttribute(PDO::ATTR_ERRODE, PDO::ERRMODE_EXCEPTION);
            
            $sql = "DELETE FROM taskTable WHERE id=:id";
            
             try
            {
                $query = $db->prepare($sql);
                $query->bindParam(":id", $id);
                $query->execute();
                
                $retVal = $query->rowCount();
            }
            catch(Exception $ex)
            {
                echo "{$ex->getMessage()}<br/>\n";
            }
            
            return $retVal;
        }
        
    }
?>
