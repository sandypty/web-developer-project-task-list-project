<?php
    require_once("TaskManager.php");
    
    $httpVerb = $_SERVER['REQUEST_METHOD'];
    
    $taskManager = new TaskManager();
    
    switch ($httpVerb)
    {
    
        case "POST":

        
            if (isset($_POST['task']) && isset($_POST['completion']))
            {
                $task = $_POST['task'];
                $completion = $_POST['completion'];

                echo $taskManager->create($task, $completion);
            }
            else
            {
                throw new Exception("Invalid HTTP POST request parameters.");
            }
                
            
            
        
        break;



        case "GET":

            header("Content-Type: application/json");
            if (isset($_GET['id'])) // Read (by Id)
            {
                echo $taskManager->readById($_GET['id']);
            }
            else
            {
                echo $taskManager->read();
            }
        break;

        case "PUT":

        parse_str(file_get_contents("php://input"), $putVars);

            if (isset($putVars['id']))
            {
                $id = $putVars['id'];
                $task = $putVars['task'];
                $completion = $putVars['completion'];

                echo $taskManager->update($id, $task, $completion);
            }
            else
            {
                throw new Exception("Invalid HTTP PUT request parameters.");
            }

        break;

        case "DELETE":

            parse_str(file_get_contents("php://input"), $deleteVars);

            if (isset($deleteVars['id']))
            {
                $id = $deleteVars['id'];

                echo $taskManager->delete($id);
            }
            else
            {
                throw new Exception("Invalid HTTP DELETE request parameters.");
            }
        break;

        default:
                throw new Exception("Unsupported HTTP request");
        break;

    }
?>
