<?php
    require_once("TaskManager.php");
    
    $taskManager = new TaskManager;
    
    $decoded_body = json_decode($taskManager->read());
    // echo "Total number of tasks: " . count($decoded_body);
    //echo "<br />";
    
    $arraylist = json_decode($taskManager->read(), true);

    //echo "<br />";
    
    $key = array_map(function($item) {
    return $item["completion"];
    }, $arraylist);
    
    $count = array_count_values($key);
    
    //echo "Total of completed tasks: " . $cat["true"];
    //echo "<br /> <br/>";
    //echo "Total of incomplete tasks: " . $cat["false"];

    // echo "<br /> <br/>";
    //var_dump($cat);
    
    $dataPoints = array( 
    	array("y" => $count["true"],"label" => "True" ),
    	array("y" => $count["false"],"label" => "False" ),
    	
    );
      
?>

<html>
<div class="container">
<div class="card" style="width: 25rem;">
  <div class="card-body">
    <h5 class="card-title">Total Number Of Tasks: </h5>
    
    <h2> <?php echo count($decoded_body); ?>  </h2>
  </div>
</div>
<div class="card" style="width: 25rem;">
  <div class="card-body">
    <h5 class="card-title">Total Number Of Incomplete Tasks: </h5>
    
    <h2> <?php echo $count["false"]; ?>  </h2>
  </div>
</div>
<div class="card" style="width: 25rem;">
  <div class="card-body">
    <h5 class="card-title">Total Number Of Completed Tasks: </h5>
    
    <h2> <?php echo $count["true"]; ?>  </h2>
  </div>
</div>

<br />

    <div>
    <script>
    window.onload = function() {
     
    var chart = new CanvasJS.Chart("chartContainer", {
    	animationEnabled: true,
    	title:{
    		text: "Task Chart"
    	},
    	axisY: {
    		title: "Number of Tasks Completed",
    	},
    	data: [{
    		type: "bar",
    		yValueFormatString: "#,##0",
    		indexLabel: "{y}",
    		indexLabelPlacement: "inside",
    		indexLabelFontWeight: "bolder",
    		indexLabelFontColor: "white",
    		dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
    	}]
    });
    chart.render();
     
    }
    </script>
 
    <div id="chartContainer" style="height: 370px; width: 100%;"></div>
    <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
    </div>
    </div>
    </html>                              


