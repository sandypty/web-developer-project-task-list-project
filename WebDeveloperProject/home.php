<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
        
        <title>Task List</title>
    </head>
    <body>

        <div class="container">
            <h1>Welcome to The Task List Project</h1>
            
            <div class="row">
  <div class="col-3">
    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
      <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">Home</a>
      <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">Dashboard</a>
      <a class="nav-link" href="taskService.php">JSON List</a>

      
    </div>
  </div>
  <div class="col-9">
    <div class="tab-content" id="v-pills-tabContent">
      <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab"> 
      
      <form id="myForm"  method="POST">
            <div class="form-group">
                <label for="addNewTask">Add A New Task</label>
                <input type="text" name ="task" id="task" class="form-control" placeholder="Enter Task">
                </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="completion" id="completion" value="true"  >
                <label class="form-check-label" for="exampleRadios1">
                Complete
                </label>
            </div>
                <div class="form-check">
                <input class="form-check-input" type="radio" name="completion" id="completion" value="false" checked>
                <label class="form-check-label" for="exampleRadios2">
                Incomplete
                </label>
            </div>
            <br/>
            <button type="submit" name= "submit" class="button" >Add Task</button>
        </form>
        
        <p id="showData"></p>
        
        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <script src="jquery-3.1.1.min.js"></script>
        <script>
        
        $(document).ready(function() {
        
            $(".button").click(function() {
                var task = $("#task").val();
                var completion = $("input[name='completion']:checked").val();
                if(task == "") {
                    alert("Please fill in task!");
                    return false;
                }
                
                var dataString = 'Task: ' + task + ' Completion: ' + completion;
                //var json = JSON.stringify(dataString);
                //alert(dataString);
                
                $.ajax({
                    type: "POST",
                    url: "taskService.php",
                    data: {task : task, completion: completion},
                    success: function() {
                        $('#showData').html(dataString);
                            
                    }
                });
                
               return false;
                
                
            });
        });
            
         
        </script>
      </div>
      <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
      <?php 
      include('dashboard.php');
      ?>
      </div>
      
  </div>
</div>
            
           


            
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
            </div>
    </body>
</html>
