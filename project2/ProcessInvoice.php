<?php
    require_once('Invoice.php');
    class ProcessInvoice 
    {
        //attibute
        private $invoice; //of type/class invoice
        
        //getters and setters
        public function getInvoice()
        {
            return $this->invoice;
        }
        
        public function setInvoice($invoice)
        {
            $this->invoice = $invoice;
        }
        
        //methods
        public function createInvoiceItem() 
        {
            $item1 = new InvoiceItem();
            $item2 = new InvoiceItem();
            $item3 = new InvoiceItem();
            
            $items = array($item1, $item2, $item3);
            
            $item1->setItemId(123);
	        $item1->setQuantity(5);
	        $item1->setPrice(12.99);
	        $item1->setDescription("Widgets");
	
	        $item2->setItemId(234);
	        $item2->setQuantity(3);
	        $item2->setPrice(6.99);
	        $item2->setDescription("Screws");
		
	        $item3->setItemId(456);
	        $item3->setQuantity(4);
	        $item3->setPrice(5.93);
	        $item3->setDescription("Nails");
	        
	        $this->invoice->setInvoiceItem($items);
        }
        
        public function runProcess()
        {
        //instantiate invoice (dont use contructors/ using lazy loading)
        //Call the createInvoiceItems() method.
        //Call the calculateInvoice() in the Invoice object.
        //Call the displayInvoice() in the Invoice object.
            $this->invoice = new Invoice();
            echo $this->createInvoiceItem();
            echo $this->invoice->calculateInvoice();
            echo $this->invoice->displayInvoice();
            
        
        }
        
    }

?>
