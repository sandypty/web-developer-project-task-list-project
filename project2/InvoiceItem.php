<?php
    class InvoiceItem
    {

        //attirbutes
        private $itemId;
        private $quantity;
        private $price;
        private $description;

        //getters/setters
        public function getItemId()
        {
            return $this->itemId;
        }
        
        public function setItemId($itemId)
        {
            $this->itemId = $itemId;
        }
        
        public function getQuantity()
        {
            return $this->quantity;
        }
        
        public function setQuantity($quantity)
        {
            $this->quantity = $quantity;
        }
        
        public function getPrice()
        {
            return $this->price;
        }
        
        public function setPrice($price)
        {
            $this->price = $price;
        }
        
        public function getDescription()
        {
            return $this->description;
        }
        
        public function setDescription($description)
        {
            $this->description = $description;
        }
        
        
        //method to calculate invoice item
        public function calculatedItemTotal()
        {   
            $calculatedItemTotal = $this->quantity * $this->price;
            
            return $calculatedItemTotal;
        }
        
        
        public function display()
        {   
            return "Item Id: " . $this->itemId . ", Item Quantity: " .
	            $this->quantity . ", Item Price: $" . $this->price . ", Item Description: " .
	            $this->description . ", Total Cost: $" . $this->calculatedItemTotal() . "<br/>";

        }
    
    }
?>
