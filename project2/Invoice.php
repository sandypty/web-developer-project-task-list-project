<?php
    require_once('InvoiceItem.php');
    class Invoice
    {
        //attributes 
        private $invoiceItem; //contains InvoiceItems(s)
        private $totalInvoice; //total cost of all  InvoiceItems

        
        public function getTotalInvoice()
        {
            return $this->totalInvoice;
        }

        public function setTotalInvoice($totalInvoice)
        {
            $this->totalInvoice = $totalInvoice;
        }

        public function getInvoiceItem()
        {
            return $this->invoiceItem;
        }

        public function setInvoiceItem($invoiceItem)
        {
            $this->invoiceItem = $invoiceItem;
        }

        //methods
        public function calculateInvoice()
        {
            //Calculate total cost of all invoiceItems(set $total) loop through  all invoice items
            //accumlate total
            $this->totalInvoice = 0;
            foreach ($this->invoiceItem as $item)
            {
                $this->totalInvoice += $item->calculatedItemTotal();
            }
        }
        
        public function displayInvoice()
        {
        //loop through invoice items
        //display each one of them
        
            //for ($counter = 0; $counter < count($this->invoiceItem); $counter++)
            //{
            //    echo $this->invoiceItem[$counter]->$display;
           // }
           foreach ($this->invoiceItem as $item)
           {
                echo $item->display();
           }
            
            echo "Invoice Total: $" . $this->totalInvoice;
        }
    }  
?>
