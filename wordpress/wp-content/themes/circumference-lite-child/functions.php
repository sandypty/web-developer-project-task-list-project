<?php

function say_hello() {
    echo "Hello World!<br/>";
}

add_shortcode('sayHello', 'say_hello');

function output_message($message) {
    echo 'Here is your message: ' . $message[0] . '<br/>';

}

add_shortcode('message', 'output_message');

function add_two_numbers($numbers) {
    $sum = $numbers[0] + $numbers[1];
    
    echo $numbers[0] . ' + ' . $numbers[1] . ' = ' . $sum . '<br/>';
}

add_shortcode('addTwoNumbers', 'add_two_numbers');

function my_theme_enqueue_styles() {

    $parent_style = 'parent-style'; // This is 'twentyfifteen-style' for the Twenty Fifteen theme.

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
}
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
?>
