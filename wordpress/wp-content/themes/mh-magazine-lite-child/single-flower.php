<?php get_header(); ?>
<div class="mh-wrapper mh-clearfix">
	<div id="main-content" class="mh-content" role="main" itemprop="mainContentOfPage"><?php
		while (have_posts()) : the_post();
			mh_before_post_content();
			get_template_part('content', 'single-flower');
			mh_after_post_content();
			comments_template();
		endwhile; 
		
		$query = array('post_type' => 'flower', 'orderby' => 'title', 'order' => 'ASC');
                    $movies = new WP_Query($query);
                    
                    if ($movies->have_posts()) {
                        echo '<ul>';
                        
                        while ($movies->have_posts()) {
                            $movies->the_post();
                            echo '<li>' . get_the_title() . '</li>';
                        }
                        
                        echo '</ul>';
                        wp_reset_postdata();
                    }
		
		?>
	</div>
	<?php get_sidebar(); ?>
</div>
<?php get_footer(); ?>
