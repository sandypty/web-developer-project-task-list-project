<?php /* Default template for displaying content. */ ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header mh-clearfix"><?php
		the_title('<h1 class="entry-title">', '</h1>');
		mh_post_header(); ?>
	</header>
	<?php dynamic_sidebar('posts-1'); ?>
	<div class="entry-content mh-clearfix"><?php
		mh_magazine_lite_featured_image();
		the_content(); ?>
		
		<?php
		    echo "<strong>Description: </strong>";
		    echo the_field('description') . "<br/>";
		    echo "<strong>Price: </strong>$";
			echo the_field('price') . "<br/>";
			echo "<strong>Width: </strong>";
			echo the_field('width') . " inches<br/>";
			echo "<strong>Height: </strong>";
			echo the_field('height') . " inches<br/>";
		?>
		
		
	</div><?php
	the_tags('<div class="entry-tags mh-clearfix"><i class="fa fa-tag"></i><ul><li>','</li><li>','</li></ul></div>');
	dynamic_sidebar('posts-2'); ?>
</article>
